<?php
if (isset($_POST['cek'])) {
    if (isset($_POST["username"]) && isset($_POST["password"])) {
        include 'helper/koneksi.php';

        $user = $conn->real_escape_string($_POST["username"]);
        
        $password = $conn->real_escape_string($_POST["password"]);

        $query = "SELECT * FROM pengguna WHERE (username='$user' OR email='$user') AND password='$password'";
        $result = $conn->query($query);

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $status = $row['user_role_id'];
            $photo = $row['photo'];
            $id_akun = $row['id'];


            if ($status == 1) {
                session_start();
                $_SESSION["username_admin"] = $user;
                $_SESSION["photo_admin"] = $photo;
                $_SESSION["id_akun_admin"] = $id_akun;
                header("Location: admin/index.php?view=home");
            } elseif ($status == 2) {
                session_start();
                $_SESSION["username_member"] = $user;
                $_SESSION["photo_member"] = $photo;
                $_SESSION["id_akun_member"] = $id_akun;
                header("Location: pengguna/index.php?view=home");
            }
            exit();
        } else {
            echo "<script>alert('Username atau password salah. Silakan coba lagi.');</script>";
        }
        $conn->close();
    } else {
        $error_message = "Mohon masukkan username/email dan password.";
    }
}
?>

<?php
include 'template/header.php';
?>

<div class=" py-5">
    <div class="container">
        <div class="row ">
            <div class="col-md-8 comp-grid">
                <div class="">
                    <div class="fadeIn animated mb-4">
                        <div class="text-capitalize">
                            <h2 align="center" class="text-capitalize">Aplikasi Arsip <br>Surat Berbasis WEB </h2>

                            <div class="">
                                <div>
                                    <center><img src="assets/images/logo.png" width="150" height="160" /></center>
                                </div>
                                <div><br>
                                    Selamat datang di Sistem Arsip Surat Berbasis Web. Sistem ini merupakan sistem yang
                                    digunakan untuk mencatat surat masuk dan surat keluar dan disertai dengan bukti
                                    upload berkas</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 comp-grid">
                <div class="bg-light p-3 animated fadeIn page-content">
                    <div>
                        <h4><i class="fa fa-key"></i> User Login</h4>
                        <hr />
                        <form action="" class="needs-validation form page-form" method="post" id="loginForm">
                            <div class="input-group form-group">
                                <input placeholder="Username Or Email" name="username" required="required"
                                    class="form-control" type="text" id="usernameInput" />
                                <div class="input-group-append">
                                    <span class="input-group-text"><i
                                            class="form-control-feedback fa fa-user"></i></span>
                                </div>
                            </div>
                            <div class="input-group form-group">
                                <input placeholder="Password" required="required" v-model="user.password"
                                    name="password" class="form-control " type="password" id="passwordInput" />
                                <div class="input-group-append">
                                    <span class="input-group-text"><i
                                            class="form-control-feedback fa fa-key"></i></span>
                                </div>
                            </div>
                            <div class="row clearfix mt-3 mb-3">
                                <div class="col-6">
                                    <label class="">
                                        <input value="true" type="checkbox" name="rememberme" id="rememberMeCheckbox" />
                                        Ingat Saya
                                    </label>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <input class="btn btn-primary btn-block btn-md" value="Login" name="cek" type="submit">
                            </div>
                            <hr />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function rememberMe() {
        var username = document.getElementById("usernameInput").value;
        localStorage.setItem("rememberedUsername", username);
    }

    function loadRememberedUsername() {
        var rememberedUsername = localStorage.getItem("rememberedUsername");
        if (rememberedUsername) {
            document.getElementById("usernameInput").value = rememberedUsername;
            document.getElementById("rememberMeCheckbox").checked = true;
        }
    }

    window.onload = function () {
        loadRememberedUsername();
    };

    document.getElementById("rememberMeCheckbox").addEventListener("change", function () {
        if (this.checked) {
            rememberMe();
        } else {
            localStorage.removeItem("rememberedUsername");
        }
    });

    document.getElementById("loginForm").addEventListener("submit", function (event) {
        if (document.getElementById("rememberMeCheckbox").checked) {
            rememberMe();
        }
    });
</script>



<?php
include 'template/footer.php';
?>